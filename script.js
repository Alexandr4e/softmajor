$(document).ready(function() {
	$('body').addClass('loaded');

	/* Up */

	(function() {
		var $arr = $('#go-up');

		$(window).scroll(function() {

			if ($(this).scrollTop() > 200 && $(window).width() > 968) {
				$arr.fadeIn();
			} else {
				$arr.fadeOut();
			}
		});
		$arr.click(function() {
			$('body,html').animate({
				scrollTop: 0
			}, 400);
		});



	})();

});